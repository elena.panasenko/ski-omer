# *Проект: "Ski-omer"*

## Описание проекта:

Задача нашей команды заключалась в том, чтобы усовершенствовать внешнешний вид страниц сайта клуба ветеранов Морского Космического Флота, сверстать их и наполнить их контентом-в нашем случае добавить базу данных содержащую информацию об участниках клуба (ФИО, дни рождения, перечни судов, на которых были совершены экспедиционные рейсы и тд.). 


* https://drive.google.com/open?id=1dgLI__JEFzULkpJ5Ma2WFDXqFk-DNPwM -презентация 
* https://drive.google.com/open?id=1zf4Re0qMEV1_Elmq_Bp5ZtaZ1dyAhDq3 -пояснительная записка
* http://str-skiomer.std-784.ist.mospolytech.ru/page3.php -страницы сайта
* http://ski-omer.std-819.ist.mospolytech.ru/index.php -база данных


## Участники
| Учебная группа | Имя пользователя | ФИО                      |
|----------------|------------------|--------------------------|
|171-334|@Anastasia|Павлова Анастасия Павлова|
|171-334|@elena.panasenko|Панасенко Елена Викторовна|
|171-331|@max.promyslov|Промыслов Максим Игоревич|
|171-331|@Korolyov|Королев Степан Максимович|
|171-331|@paparimskiy|Тюкин Роман Викторович|


 
## Личный вклад участников:
### Панасенко Елена Викторовна

* Разработка(верстка) страниц сайта в объеме 60 - часов;
* Подготовка всей документации и презентации к защите проекта в объеме - 5 часов;
* Решение проблем с размещением файлов на сервере Политеха в объеме - 4 часов.

### Павлова Анастасия Павловна

* Добавление базы данных на страницы сайта в объеме 50 - часов;
* Доработка кода созданной коллегами из 181-331 базы данных в объеме - 3 часов;
* Решение проблем с размещением файлов на сервере Политеха в объеме - 4 часов.


### Промыслов Максим Игоревич

* Разработка бэкэнда страниц "Руководители", "Люди", "Дни рождения" в объеме - 40 часов;
* Разработка документации и презентации по части бэкэнда к защите проекта в объеме - 2,5 часов;
* Тестирование готового проекта в объеме - 2,5 часов.

### Королев Степан Максимович

* Разработка функции поиска в объеме - 50 часов;
* Подготовка документации и презентации по части бэкэнда в объеме - 2,5 часов;
* Тестирование готового проекта в объеме - 2,5 часов.

### Тюкин Роман Викторович

* Разработка бэкэнда страниц профилей в объеме - 45 часов;
* Подготовка документации и презентации по части бэкэнда в объеме - 2,5 часов;
* Тестирование готового проекта в объеме - 2,5 часов.
