<!--Footer-->
<footer class="page-footer mdb-color darken-3 text-center text-md-left pt-5">

    <!--Footer Links-->
    <div class="container mb-3">

        <!--First row-->
        <div class="row">

            <!--First column-->
            <div class="col-md-6 mt-1 mb-1 wow fadeIn" data-wow-delay="0.3s">
                <!--About-->
                <h5 class="title mb-4 font-weight-bold">МОРСКОЙ КОСМИЧЕСКИЙ ФЛОТ</h5>


                <p>  Ветераны космического флота гордятся тем, что служили и работали на флоте,
				созданном умом и руками многих тысяч советских судостроителей, конструкторов, рабочих, инженеров на базе отечественной техники и электроники.</p>
                <!--/About -->

                <div class="footer-socials mt-2">

                    <!--Facebook-->
                    <a type="button" class="btn-floating  btn-danger-2"><i class="fab fa-facebook-f"></i></a>
                    <!--Dribbble-->
                    <a type="button" class="btn-floating  btn-danger-2"><i class="fab fa-dribbble"></i></a>
                    <!--Twitter-->
                    <a type="button" class="btn-floating  btn-danger-2"><i class="fab fa-twitter"></i></a>
                    <!--Google +-->
                    <a type="button" class="btn-floating  btn-danger-2"><i class="fab fa-google-plus-g"></i></a>

                </div>
            </div>
            <!--/First column-->

            <hr class="w-100 clearfix d-md-none">

            <!--Second column-->
            <div class="col-md-6 ml-lg-auto col-md-4 mt-1 mb-1 wow fadeIn" data-wow-delay="0.3s">
                <!--Search-->
                <h5 class="text-uppercase mb-4 font-weight-bold">Контакты</h5>

                <!--Info-->
                <p><i class="fas fa-home pr-1"></i> Москва, Россия</p>
                <p><i class="fas fa-envelope pr-1"></i>Присылайте свои письма и материалы по обсуждаемой теме на адрес: qwert505@bk.ru</p>
                <p><i class="fas fa-phone pr-1"></i> Звоните Председателю Совета РОО «Клуб ветеранов МКФ» Анатолию Капитанову. Тел: (917) 507-75-11</p>
                <p><i class="fas fa-print pr-1"></i> E-mail Kлуба: kapitanov2006@yandex.ru</p>

            </div>
            <!--/Second column-->

           

        </div>
        <!--/First row-->

    </div>
    <!--/Footer Links-->

    <!--Copyright-->
    <div class="footer-copyright py-3 text-center">
        <div class="container-fluid">
            © 2019 <a href="https://mdbootstrap.com/education/bootstrap/" target="_blank">
            Морской космический флот, 2008-2019 </a>
        </div>
    </div>
    <!--/Copyright-->

</footer>
<!--/Footer-->


<!-- SCRIPTS -->

<!-- JQuery -->
<script type="text/javascript" src="source/vendor/jquery-3.3.1.min.js"></script>

<!-- Bootstrap tooltips -->
<script type="text/javascript" src="source/vendor/popper.min.js"></script>

<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="source/vendor/bootstrap.min.js"></script>

<!-- MDB core JavaScript -->
<script type="text/javascript" src="source/vendor/mdb.min.js"></script>

<script>
    //Animation init
    new WOW().init();

</script>

</body>

</html>
