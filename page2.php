<?php

include 'header.php';
if (isset($_GET['pageno'])) {
	$pageno = $_GET['pageno'];
} 
else {
	$pageno = 1;
}
$no_of_records_per_page = 6; //количество элементов на одной странице
$offset = ($pageno-1) * $no_of_records_per_page; //сдвиг

$conn=mysqli_connect("std-mysql","std_819","12345678","std_819");
// Check connection
if (mysqli_connect_errno()){
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
	die();
}
					
$total_pages_sql = "SELECT COUNT(*) FROM Profiles";
$result = mysqli_query($conn,$total_pages_sql);
$total_rows = mysqli_fetch_array($result)[0];
$total_pages = ceil($total_rows / $no_of_records_per_page);
?>
<!--Main content-->
<main>
    <!--First container-->
    <div class="container">

        <!--Section: About-->
        <!--        Потом стереть style, когда добавите контент-->
        <section class="mt-5 mb-5 text-center">
		<div class="row text-center d-flex">
	<div class="col-12">
	
	<div class="tittle d-flex justify-content-around">
	<h1>Люди</h1>
			<form action="result.php" method="GET" class="form-inline">
						<input class="form-control" type="text" name="name" placeholder="Поиск"></input>
						<button name="submit" class="btn" style="background-color: #24355c!important; color: white">Искать</button>
					</form>
			</div>
	<p>Страница <?=$pageno?> из <?=$total_pages?></p>
		<?php
		$sql = "SELECT * FROM Profiles LIMIT $offset, $no_of_records_per_page";
		$res_data = mysqli_query($conn,$sql);
		?>
			<table class="table table-striped table-bordered">
			<thead>
			<tr>
				<th>Фамилия, имя, отчество</th>
				<th>На каких судах ходил(а)</th>
				<th>Экс/экип</th>
			</tr>
			</thead>
			<tbody>
						
		<?php
			while($row = mysqli_fetch_array($res_data)){
		?>
			<tr>
				<td><a href="/profile.php?id=<?=$row[id]?>"><?=$row[last_name]?></b><br><?=$row[first_name]?> <?=$row[patronymic]?></a></td> 
				<td><?=$row[description]?></td>
				<td><?=$row[profession]?></td>
			</tr>
		<?php
			}
		mysqli_close($conn);
		?>
			</tbody>
			</table>		
	</div>

<div class="col-12 d-flex text-center justify-content-center">
				<div><a href="?pageno=1" class="btn btn-light">Первая</a></div>
				<div>
					<a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>" class="btn btn-light">Предыдущая</a>
				</div>
				<div >
					<a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>" class="btn btn-light">Следующая</a>
				</div>
				<div><a href="?pageno=<?php echo $total_pages; ?>" class="btn btn-light">Последняя</a></div>
							
			</div>	
		</section>
	</div>
</main>
<!--/Main content-->


<?php

include 'footer.php';

?>