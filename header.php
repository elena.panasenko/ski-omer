<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Морской космический флот</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="source/vendor/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="source/vendor/mdb.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="source/img/logo.png" type="image/x-icon">
    <style>
        html,
        body,
        header,
        .jarallax {
            height: 100%;
        }

        @media (min-width: 560px) and (max-width: 740px) {
            html,
            body,
            header,
            .jarallax {
                height: 500px;
            }
        }

        @media (max-width: 560px) {
            .tittle1 {
                font-size: 40px;
            }
            .search-block
            {
                display: none;
            }

        }


        @media (min-width: 800px) and (max-width: 850px) {
            html,
            body,
            header,
            .jarallax {
                height: 600px;
            }
        }

        @media (min-width: 800px) and (max-width: 850px) {
            .navbar:not(.top-nav-collapse) {
                background: #24355c !important;
            }

            .navbar {
                box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .16), 0 2px 10px 0 rgba(0, 0, 0, .12) !important;
            }
        }


        nav {
            font-size: 14.5px;
        }
    </style>
</head>

<body class="intro-page transport-lp">

<!--Navigation & Intro-->
<header>

    <!--Navbar-->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
        <div class="container">
            <!--            <a class="navbar-brand" href="#"><strong style="text-transform: uppercase;">ski-omer</strong></a>-->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!--Links-->
                <ul class="navbar-nav mr-auto smooth-scroll">
                    <li class="nav-item">
                        <a class="nav-link" href="index">Главная <span class="sr-only">(current)</span></a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#" data-offset="100">Память о наших товарищах</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-offset="30">РОО «Клуб ветеранов МКФ»</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-offset="100">Форум</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-offset="30">Контакты</a>
                    </li>
					<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
      aria-expanded="false">Данные</a>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="index.php">Руководители</a>
      <a class="dropdown-item" href="page2.php">Люди</a>
      <a class="dropdown-item" href="page3.php">Дни рождения</a>
  </li>
                </ul>

                <!--Search Menu-->
                <ul class="navbar-nav nav-flex-icons">
                    <form class="form-inline ml-auto search-block">
                        <div class="md-form my-0">
                            <input class="form-control" type="text" placeholder="Поиск" aria-label="Поиск">
                        </div>
                        <button href="#" class="btn btn-outline-white btn-md my-0 ml-sm-2" type="submit">Поиск</button>
                    </form>
                </ul>
            </div>
        </div>
    </nav>
    <!--/Navbar-->

    <!-- Intro Section -->
    <div id="home" class="view jarallax" data-jarallax='{"speed": 0.2}'
         style="background-image: url(source/img/background.jpg); background-repeat: no-repeat; background-size: cover; background-position: center center;">
        <div class="mask rgba-black-light">
            <div class="container h-100 d-flex justify-content-center align-items-center">
                <div class="row smooth-scroll">
                    <div class="col-md-12 white-text text-center">
                        <div class="wow fadeInDown" data-wow-delay="0.2s">
                            <h1 class="display-4 text-uppercase px-3 py-2"><strong
                                    class="font-weight-bold tittle1">Морской космический флот</strong></h1>
                            <h4 class="white-text mt-5 tittle2">Отдел морских экспедиционных работ академии наук
                                ссср</h4>
                            <!--                            <h2 class="white-text text-uppercase h2-responsive font-weight-bold mb-5 mt-4">Описание</h2>-->
                            <!--                            <a href="#getaquote" data-offset="100" class="btn btn-danger-2 btn-rounded">Ссылка</a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header>
<!--/Navigation & Intro-->